This is code that adds Dungeons back to cookie clicker. This code is intended for versions 2.031 and 2.048.  Instructions will be listed below.

======================================
Bookmarklet Guide
======================================
Step 1: Create a new bookmark that you are willing to overwrite.

Step 2: Copy this code: javascript:function(Game.LoadMod(https://codeberg.org/Number_1_rated_salesman1997/Cookie_Clicker_Live_Dungeons/main/index.html))

Step 3: Go the the bookmark edit screen

Step 4: Replace the URL with the code from step 2, and name it whatever you prefer

Step 5: Go to cookie clicker (Make sure it's version 2.048 or 2.031) and click the bookmark


======================================
Console Guide
======================================
Step 1: Open your browser's JS Console

Step 2: Paste the following code into the console: Game.LoadMod(https://codeberg.org/Number_1_rated_salesman1997/Cookie_Clicker_Live_Dungeons/main/index.html)

======================================
Steam Guide
======================================
Step 1: Download the index.html file

Step 2: Go to the cookie clicker steam files

Step 3: Upload the index.html file into the steam mod folder

___________________________________________________________________________________________________________________________________
- Overview

Orteil added dungeons to a beta version 1.037, seperate from the main game. Despite being seperate, the dungeon's files were added to the main files into the game yet left unused and neglected. Many people have implored Orteil to continue work on them due to how loved they were by the community, but he ceased production on them, despite having the files in the live versions. Because of the lack of features the beta contains over the live version, the dungeons became the only real interest in the beta and made people desire it in the live version more and more. With no way to obtain them, however, people have been left with no way of naturally obtaining them in the live version, until now.